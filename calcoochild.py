#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Simple calculator (OO version with 4 operations).

This module provides a class Calc, with four functions,
add, sub, mul, and div to add, substract multiply or divide two operands.
The main code of the module taks operands and operation (+ or -)
from the command line, as arguments when running the program.
The format for calling the module from the command line is:
calc.py op1 op op2
For example:
calcoo.py 4 + 7
11
"""

import sys
import calcoo


class Calc(calcoo.Calc):

    def mul(self, op1, op2):
        """ Function to substract the operands """
        return op1 * op2

    def div(self, op1, op2):
        """ Function to substract the operands """
        try:
            result = op1 / op2
        except ZeroDivisionError:
            print("Division by zero is not allowed")
            raise ValueError("Division by zero is not allowed")
        return result


if __name__ == "__main__":
    try:
        operand1 = float(sys.argv[1])
        operand2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: first and third arguments should be numbers")

    calc = Calc()
    if sys.argv[2] == "+":
        result = calc.add(operand1, operand2)
    elif sys.argv[2] == "-":
        result = calc.sub(operand1, operand2)
    elif sys.argv[2] == "x":
        result = calc.mul(operand1, operand2)
    elif sys.argv[2] == "/":
        try:
            result = calc.div(operand1, operand2)
        except ValueError:
            result = "Error"
    else:
        sys.exit('Operand should be +, -, x or /')

    print(result)
