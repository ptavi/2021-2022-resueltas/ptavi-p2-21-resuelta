#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Program to compute operations defined in a CSV file.
Each line of the file corresponds to a series of operations,
with the format:

operation,operand1,operand2,operand3,...

This will be evaluated as
(((operand1 operation operand2) operation operand2) operation operand3) ...
"""

import sys
import calccount

calc = calccount.Calc()
ops = {
    '+': calc.add,
    '-': calc.sub,
    'x': calc.mul,
    '/': calc.div
}


def process_line(line):
    contents = line.rstrip().split(',')
    if len(contents) < 3:
        raise ValueError
    else:
        operand = contents.pop(0)
        result = float(contents.pop(0))
        while len(contents) > 0:
            op2 = float(contents.pop(0))
            result = ops[operand](result, op2)
        print(result)


def process_csv(filename):
    with open(filename, 'r') as file:
        for line in file:
            try:
                process_line(line)
            except (ValueError, KeyError):
                print("Bad format")


if __name__ == "__main__":
    try:
        filename = sys.argv[1]
    except IndexError:
        print("Error: Execute with the name of file to process as argument")
        sys.exit(-1)
    try:
        process_csv(filename)
    except FileNotFoundError:
        print(f"Error: File {filename} does not exist")
        sys.exit(-1)
