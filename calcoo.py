#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Simple calculator (OO version).

This module provides a class Calc, with two functions,
add and sub, to add or subtract two operands.
The main code of the module takes operands and operation (+ or -)
from the command line, as arguments when running the program.
The format for calling the module from the command line is:
calc.py op1 op op2
For example:
calcoo.py 4 + 7
11
"""

import sys


class Calc():

    def add(self, op1, op2):
        """ Function to add the operands"""
        return op1 + op2

    def sub(self, op1, op2):
        """ Function to subtract the operands """
        return op1 - op2


if __name__ == "__main__":
    try:
        operand1 = float(sys.argv[1])
        operand2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: first and third arguments should be numbers")

    calc = Calc()
    if sys.argv[2] == "+":
        result = calc.add(operand1, operand2)
    elif sys.argv[2] == "-":
        result = calc.sub(operand1, operand2)
    else:
        sys.exit('Operand should be + or -')

    print(result)
