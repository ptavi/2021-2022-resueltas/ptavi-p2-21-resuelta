#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Simple calculator (OO version with 4 operations and count).

This module provides a root class Calc, with four functions,
add, sub, mul, and div to add, subtract multiply or divide two operands,
and a instance variable count, with the count of invocations of
functions.
"""


class Calc():

    def __init__(self):
        """ Initialize count """
        self.count = 0

    def _increment(self):
        """ Increment count """
        self.count += 1

    def add(self, op1, op2):
        """ Function to add the operands """
        self._increment()
        return op1 + op2

    def sub(self, op1, op2):
        """ Function to subtract the operands """
        self._increment()
        return op1 - op2

    def mul(self, op1, op2):
        """ Function to subtract the operands """
        self._increment()
        return op1 * op2

    def div(self, op1, op2):
        """ Function to subtract the operands """
        self._increment()
        try:
            result = op1 / op2
        except ZeroDivisionError:
            print("Division by zero is not allowed")
            raise ValueError("Division by zero is not allowed")
        return result
